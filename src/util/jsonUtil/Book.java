package util.jsonUtil;

/**
 * class that have exemplar of one book
 */
public class Book {

    /**
     * this class fields are book attribute
     */
    private String author;
    private String data;
    private String price;
    private String genre;
    private String count;
    private String title;
    private String language;

    public String getAuthor() {
        return author;
    }

    /**
     * util method to set author name
     *
     * @param author author of book
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    public String getData() {
        return data;
    }

    /**
     * util method to set data value
     *
     * @param data data that book write
     */
    public void setData(String data) {
        this.data = data;
    }

    public String getPrice() {
        return price;
    }

    /**
     * util method to set price value
     *
     * @param price selling price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    public String getGenre() {
        return genre;
    }

    /**
     * util method to set genre of current books
     *
     * @param genre genre of book
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getCount() {
        return count;
    }

    /**
     * util method to set count of books
     *
     * @param count
     */
    public void setCount(String count) {
        this.count = count;
    }

    public String getTitle() {
        return title;
    }

    /**
     * method that set title of books
     *
     * @param title name of book
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public String getLanguage() {
        return language;
    }

    /**
     * set language of books
     *
     * @param language book written in this language
     */
    public void setLanguage(String language) {
        this.language = language;
    }
}
