package util.jsonUtil;

import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

/**
 * create json file in user computer
 */
public class JsonFileWriter {

    /**
     * create resulted file
     *
     * @param file json file
     */
    public JsonFileWriter(JSONObject file) {
        try (FileWriter fileWriter = new FileWriter("example.json")) {
            fileWriter.write(file.toString());
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Something went wrong");
        }
        System.out.println("Done!");
    }
}
