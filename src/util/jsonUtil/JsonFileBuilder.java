package util.jsonUtil;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Create and fill structure of future JSON file
 */
public class JsonFileBuilder {

    /**
     * jsonObject with parameters from parsed xml-file
     */
    private JSONObject parameters = new JSONObject();
    /**
     * list which save data about all book
     */
    private List<Book> saveBook = new ArrayList<>();

    /**
     * Build json file. To in get data from xml-file, to input sends formed jsonFile with received information.
     *
     * @param data parced data from xml-file
     */
    public JsonFileBuilder(List<List> data) {
        JSONObject root = new JSONObject();//create root element of json-file
        JSONObject books = new JSONObject();//element which save data of books

        for (int i = 0; i < data.get(0).size(); i++)
            buildStructure(i, data);//call method that build structure of future file

        for (Book aSaveBook : saveBook) {
            JSONObject temp = new JSONObject(aSaveBook);//create temporary file that save info about one book
            books.append("books", temp);//append info about one book to all data about book
        }
        root.put("bookstore", books);//formed final structure of json file
        new JsonFileWriter(root);//write the result file
    }

    /**
     * Build structure of future json file. Parameters fill their fields.
     *
     * @param index      number of element of their data-list
     * @param parcedData data-list with parsed data from xml-file
     */
    private void buildStructure(int index, List<List> parcedData) {
        if (!filterData("ru", (String) parcedData.get(0).get(index)))//this station are tested, if we want take lang ru or wasn't we can just add "!"
        {
            /**
             * in this section parameters from list of data fill own data in jsonObject
             */
            getParameters().put("title", parcedData.get(6).get(index));
            getParameters().put("language", parcedData.get(0).get(index));
            getParameters().put("author", parcedData.get(2).get(index));
            getParameters().put("genre", parcedData.get(4).get(index));
            getParameters().put("data", parcedData.get(1).get(index));
            getParameters().put("count", parcedData.get(5).get(index));
            getParameters().put("price", parcedData.get(3).get(index));
            fillBookData(index, parcedData);//call method that fill books data
        }
    }

    /**
     * Filter information that put in json-file.
     *
     * @param filteredValue value that choose user to filtered your data that formed json file
     * @param data          String of check data
     * @return true if data equals filtered value
     */
    private boolean filterData(String filteredValue, String data) {
        return filteredValue.equals(data);
    }

    /**
     * Fill data about one book
     *
     * @param index     number of element in their data-lists
     * @param localData data-lists with books parameters
     */
    private void fillBookData(int index, List<List> localData) {
        Book book = new Book();//create book
        /**
         * in this section books take their attributes
         */
        book.setTitle((String) localData.get(6).get(index));
        book.setLanguage((String) localData.get(0).get(index));
        book.setAuthor((String) localData.get(2).get(index));
        book.setGenre((String) localData.get(4).get(index));
        book.setData((String) localData.get(1).get(index));
        book.setPrice((String) localData.get(3).get(index));
        book.setCount((String) localData.get(5).get(index));
        saveBook.add(book);//save received exemplar
    }

    /**
     * Util function
     *
     * @return jsonObject
     */
    public JSONObject getParameters() {
        return parameters;
    }

    public void setParameters(JSONObject parameters) {
        this.parameters = parameters;
    }

}
