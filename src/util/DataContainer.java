package util;

import util.jsonUtil.JsonFileBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Contain parsed data
 */
public class DataContainer {

    /**
     * Lists for data
     */
    private List<String> data = new ArrayList<>();
    private List<String> lang = new ArrayList<>();
    private List<String> author = new ArrayList<>();
    private List<String> price = new ArrayList<>();
    private List<String> genre = new ArrayList<>();
    private List<String> count = new ArrayList<>();
    private List<String> title = new ArrayList<>();

    /**
     * Filled lists of data
     */
    public void fillData() {
        List<List> allData = new ArrayList<>();//create util list which take in all lists with parameters of book
        if (lang.isEmpty() && data.isEmpty() && author.isEmpty() && price.isEmpty() && genre.isEmpty() && count.isEmpty() && title.isEmpty())
            System.out.println("Nothing to build in json-file");//check is lists are empty
        else {
            /**
             * in this section lists with parameters of book add to common list
             */
            allData.add(lang);
            allData.add(data);
            allData.add(author);
            allData.add(price);
            allData.add(genre);
            allData.add(count);
            allData.add(title);
            new JsonFileBuilder(allData);//create structure of json file with received data
        }
    }

    /**
     * Save data in own list
     *
     * @param info       received information that need to save in list
     * @param nameOfList name of list that need save information
     */
    public void saveData(String info, String nameOfList) {
        switch (nameOfList) {
            /**
             * in this section info add in own list
             */
            case "lang": {
                lang.add(info);
                break;
            }
            case "data": {
                data.add(info);
                break;
            }
            case "author": {
                author.add(info);
                break;
            }
            case "price": {
                price.add(info);
                break;
            }
            case "genre": {
                genre.add(info);
                break;
            }
            case "count": {
                count.add(info);
                break;
            }
            case "title": {
                title.add(info);
                break;
            }
            default: {
                System.out.print("no such list in program");
            }
        }
    }
}
