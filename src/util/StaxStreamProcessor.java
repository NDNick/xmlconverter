package util;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;

/**
 * util class for helped parsing xml-file
 */
public class StaxStreamProcessor implements AutoCloseable {
    /**
     * abstract implementation of a factory for getting streams
     */
    private static final XMLInputFactory factory = XMLInputFactory.newInstance();
    /**
     * designed to iterate over XML using next() and hasNext()
     */
    private final XMLStreamReader reader;

    /**
     * constructor that allow reader get access to xml file info
     *
     * @param stream inputStream with new xml file
     * @throws XMLStreamException if structure of xml file incorrect
     */
    public StaxStreamProcessor(InputStream stream) throws XMLStreamException {
        reader = factory.createXMLStreamReader(stream);
    }

    public XMLStreamReader getReader() {
        return reader;
    }

    /**
     * analysis file until stop event
     *
     * @param stopEvent param that allow now end of xml file
     * @param value     analyzed value
     * @return true if file end and value equal received in file, else return false
     * @throws XMLStreamException if structure xml file incorrect
     */
    public boolean doUntil(int stopEvent, String value) throws XMLStreamException {
        while (reader.hasNext()) {
            int event = reader.next();//add new event, iterate read process
            if (event == stopEvent && value.equals(reader.getLocalName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * util method for get attributes from xml tag
     *
     * @param name name of xml tag
     * @return value contains attributes of xml tag
     */
    public String getAttribute(String name) {
        return reader.getAttributeValue(null, name);
    }

    /**
     * util method for get text from xml tag
     *
     * @return text in xml tag
     * @throws XMLStreamException if tag corrupted
     */
    public String getText() throws XMLStreamException {
        return reader.getElementText();
    }

    /**
     * closed open xml file
     *
     * @throws Exception trouble with xml file
     */
    @Override
    public void close() throws Exception {
        if (reader != null) {
            try {
                reader.close();
            } catch (XMLStreamException e) {
                System.out.print(e);//print stack trace
            }

        }
    }
}
