package xml;

import util.DataContainer;
import util.StaxStreamProcessor;

import javax.xml.stream.events.XMLEvent;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * xml file parser
 */
public class XMLParser {

    /**
     * path to xml file which will be parsed
     */
    private String pathToXMLFile;

    /**
     * set path to xml file, parse parameters and attributes in xml file
     *
     * @param dc
     * @param path
     */
    public XMLParser(DataContainer dc, String path) {
        setPathToXMLFile(path);
        parseParameters(dc, "data");
        parseParameters(dc, "author");
        parseParameters(dc, "price");
        parseParameters(dc, "genre");
        parseParameters(dc, "count");
        parseParameters(dc, "title");
        parseAttributes(dc);
    }

    /**
     * parse attributes inside xml tags
     *
     * @param dc data container
     */
    private void parseAttributes(DataContainer dc) {
        try (StaxStreamProcessor processor = new StaxStreamProcessor(Files.newInputStream(Paths.get(getPathToXMLFile())))) {
            while (processor.doUntil(XMLEvent.START_ELEMENT, "title")) {
                dc.saveData(processor.getAttribute("lang"), "lang");
            }
        } catch (Exception e) {
            System.out.println("An error occurred when parsing the attributes\n" +
                    "Check the structure of xml-file");
            //e.printStackTrace();
            //TODO: add log write
        }
    }

    /**
     * parse parameters inside xml tags
     *
     * @param dc              data container
     * @param nameOfParameter name of parameter that need to parsed
     */
    private void parseParameters(DataContainer dc, String nameOfParameter) {
        try (StaxStreamProcessor processor = new StaxStreamProcessor(Files.newInputStream(Paths.get(getPathToXMLFile())))) {
            while (processor.doUntil(XMLEvent.START_ELEMENT, nameOfParameter)) {
                dc.saveData(processor.getText(), nameOfParameter);
            }
        } catch (Exception e) {
            System.out.println("An error occurred when parsing the parameter " + nameOfParameter + "\n" +
                    "Check the structure of xml-file");
            //e.printStackTrace();
            //TODO: add log
        }
    }

    /**
     * util method to set path
     *
     * @param pathToXMLFile
     */
    public void setPathToXMLFile(String pathToXMLFile) {
        this.pathToXMLFile = pathToXMLFile;
    }

    /**
     * util method to get path
     *
     * @return
     */
    public String getPathToXMLFile() {
        return pathToXMLFile;
    }
}
