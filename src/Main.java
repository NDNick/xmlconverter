import util.DataContainer;
import xml.XMLParser;

/**
 * main class of application
 */
public class Main {

    /**
     * take path to xml file, add parsing process
     *
     * @param args path to xml file
     */
    public static void main(String[] args) {
        final String pathToXMLFile;

        try {
            pathToXMLFile = args[0];
            //pathToXMLFile = "C:\\Users\\Nick\\IdeaProjects\\ConverterXMLToJSON\\out\\artifacts\\ConverterXMLToJSON_jar\\example.xml";

            DataContainer dc = new DataContainer();
            new XMLParser(dc, pathToXMLFile);
            dc.fillData();
        } catch (Exception e) {
            System.out.println("Enter path to file, and file name\n" +
                    "or check xml-file structure");
        }
    }
}